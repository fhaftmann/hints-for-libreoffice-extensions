
* [Bevor es losgeht](Development_Notes/01_Bevor_es_losgeht.md)

* [Grundlagen](Development_Notes/02_Grundlagen.md)

* [Organisationsprinzipien](Development_Notes/03_Organisationsprinzipien.md)

* [Designprinzipien](Development_Notes/04_Designprinzipien.md)

* [Entwicklungstechniken](Development_Notes/05_Entwicklungstechniken.md)

* [Deployment](Development_Notes/06_Deployment.md)

* [Troubleshooting](Development_Notes/08_Troubleshooting.md)
