
# Designprinzipien

Ein Manko bei office-basierten Lösungen ist, dass die Trennung von
Anwendung und Daten leicht verwaschen werden kann.
Man kann dies vermeiden, indem man auf in Dokumente eingebetteten Code
verzichtet.


## Woraus eine Office-Erweiterung besteht

* Eine Office-Erweiterung hat einen Namen, der der Konvention
`MAK_`*NNN*`_`*Titel* folgt, wobei *NNN* eine dreistellige Nummer ist.
Das Präfix `MAK` ist historisch geerbt.

* Kernstück ist eine Office-Erweiterung.

* Diese Erweiterung enthält in der Regel auch die implementierte
Funktionalität.

* Diese Erweiterung darf ein Menü oder auch eine Toolbar zum
Benutzerinterface hinzufügen.

* Die Erweiterung enthält eine Benutzerdokumentation, die über Menü
oder Toolbar abrufbar ist.

* Die Erweiterung kann Vorlagen bereitstellen.

* Diese Vorlagen dürfen auch eingebetteten Code, Menüs und Toolbars
mitbringen, auch wenn dies zu vermeiden ist.

* Ergänzendes Material für den Administrator wird separat bereitgestellt;
zu diesem Material gehört eine Dokumentation,
welche Vorbereitungen zum Einsatz des Makros nötig sind.


## Umgang mit Dokumenten und ihren Inhalten

Code in Dokumenten ist potentiell problematisch:

* Eingebetteter Code entzieht sich der Kontrolle durch die
Softwareverteilung.

* Inhalte können sensitive Daten enthalten.

Diese Probleme werden gemildert, indem eingebetteter Code sowie
Inhalte, die nicht als bedenkenlos eingestuft sind,
[verschlüsselt](#templates) werden.
