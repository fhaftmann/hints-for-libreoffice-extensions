
# Entwicklungstechniken

## Die Beispielanwendung {#example}

In *Example_and_Templates/* findet sich eine Beispielanwendung, die viele
Techniken exemplarisch demonstriert und deren Bestandteile auch als
Vorlagen verwendet werden können.


## Bau der Erweiterung {#build-extension}

Der Bau der Erweiterung eines Makros mit allen
Abhängigkeiten wird mit folgendem Aufruf angestoßen.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  ./deploy --extension-only …
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Als Argument werden diejenigen Verzeichnisse angegeben, die
gebaut werden sollen.

Damit werden die Sourcen entsprechend dem
[Dateisystem-Layout](#dateisystem-layout) gebaut.


## Dateisystem-Layout {#dateisystem-layout}

### Dateisystem-Layout für jedes Makro

Der Build-Prozess basiert auf einem standardisierten Dateisystem-Layout,
das hier in der Reihenfolge der Buildschritte dargelegt wird.

`readonly/`

:   Office-Dokumente in diesem Verzeichnis werden nach PDF
    konvertiert; nach Konvention ist hier die Dokumentation
    abgelegt: *MAK_NNN_Titel_*`UserDoku.odt` bzw.
    *MAK_NNN_Titel_*`AdminDoku.odt`.

`documents/`

:   Extrahierte Office-Dokumente in diesem Verzeichnis werden
    gepackt.  Dieses Verzeichnis kann so für eingebettete Vorlagen
    verwendet werden, die wiederum eingebetteten Code enthalten.  Alle
    anderen Dateien bleiben unverändert;  daher finden sich auch
    nicht-extrahierte eingebettete Vorlagen oft in diesem Verzeichnis.

`crypt/libs.pwd`

:   Mit diesem Passwort wird eingebetteter Code extrahierter
    Office-Dokumente in `documents/` beim Packen verschlüsselt.

`crypt/content.pwd`

:   Mit diesem Passwort werden Inhalte extrahierter Office-Dokumente
    in `documents/` beim Packen verschlüsselt.

`extension/`

:   Es gibt *genau ein* Verzeichnis mit einer extrahierten Erweiterung.
    Diese Erweiterung wird gepackt.  Ergebnisse der vorherigen
    Buildschritte werden via [Symlink](#symlink) eingebunden.

*Erweiterung*`.prebuild`

:   Benötigt die Erweiterung zum Build Abhängigkeiten, die nicht über
    die oben genannten Verzeichnisse darstellbar sind, kommt dieses
    Skript zum Einsatz, das implizit von `odoc bundle --prebuild …`
    aufgerufen wird.  Ein typischer Fall sind Java-Sourcen, deren Build
    ein Jar-Archiv für die Erweiterung produziert.  Dies kann wiederum
    via [Symlink](#symlink) eingebunden werden.

`admin/`

:   Diese Dateien sind im Zip-Archiv für den Administrator enthalten.
    Ergebnisse der vorherigen Buildschritte werden via
    [Symlink](#symlink) eingebunden.

    Die Admin-Dokumentation ist hier eingebunden.

    Weitere typische Vertreter sind Skripte zum Aufsetzen von
    SQL-Datenbanken.  Deren Suffixe folgen folgenden Konventionen:

    * `_db.sql`

    :   Datenbank erstellen und mit initialen Daten befüllen.

    * `_account.sql`

    :   Zugriffsaccount erstellen.

    * `_testdata.sql`

    :   Datenbank mit Testdaten befüllen.

    * `_proddata.sql`

    :   Datenbank mit Produktionsdaten befüllen.

Diese Konventionen sind nicht nur für den Build günstig.  Der Shell-Glob
`*/documents/*/meta.xml` liefert z. B. alle Metadaten von extrahierten
Dokumenten und der Shell-Glob `*/*/description.xml` alle Beschreibungen
von Erweiterungen.  Mit UNIX-Bordmitteln wie *find*, *grep*, *perl* usw.
sind damit ambitionierte querschnittliche Analysen möglich, oder sogar
konsistente Änderungen an allen Makros.


### Dateisystem-Layout für Sourcen, die mehreren Makros gemeinsam sind

Hierfür gibt es einen gemeinsamen Bereich unter `common/`, dessen
Bestandteile per [Symlink](#symlink) eingebunden werden können.


## Mit einzelnen Dateien arbeiten

In der Praxis stellt sich das Problem, dass die Artefakte in den Sourcen
als extrahierte Zip-Archive vorliegen; in dieser Form kann damit nicht
unmittelbar in Office gearbeitet werden.  Es gibt ein paar bewährte
Handgriffe, um mit dieser Situation klar zu kommen.


### Mit einem temporären Office-Profil arbeiten

Generell ist es empfehlenswert, Makros mit einem temporären
Office-Profil zu bearbeiten und zu testen; damit werden Seiteneffekte
durch zufällige Voreinstellungen ausgeschlossen und auch das eigene
Office-Profil nicht durch Hinterlassenschaften verunreinigt.

Ein solche Profil kann z. B. mit

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  temporary_profile=/tmp/office_profile
  libreoffice -env:UserInstallation="file://$temporary_profile"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

angelegt werden und im folgenden immer wieder verwendet bzw. durch
Löschen zurückgesetzt werden.


### Umgang mit Erweiterungen

* Erweiterungen enthalten typischerweise die Ergebnisse vorheriger
Builds; daher muss zuvor der komplette [Bau](#build-extension)
dieser Erweiterung angestoßen werden, um diese Buildergebnisse auch
vorliegen zu haben.

* Danach kann die Erweiterung wiederholt nach Bedarf neu gebaut werden.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    name=Beispielname
    odoc bundle "$name"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Die Erweiterung wird ins temporäre Profil installiert.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    unopkg add --force -env:UserInstallation="file://$temporary_profile" "$name.oxt"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Office starten

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    libreoffice -env:UserInstallation="file://$temporary_profile" "$name.oxt"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Erweiterung testen, Code anpassen etc.

* Da die Erweiterung im temporären Profil nicht als OXT vorliegt, kann
hier auch keine OXT wieder entpackt werden.  Am besten ist es, geänderten
Code aus der IDE direkt wieder in die Sourcen zurückzukopieren.

* Schließen von Office.

* Ggf. Schritte iterieren; die Erweiterung kann immer wieder über die
alte Version drüber installiert werden.

* Abschluss der Arbeiten (typischerweise `git commit`).

* Dann kann z. B. die gewöhnliche [Bereitstellung](#deployment)
angestoßen werden, um einen produktionsnahen Plausibilitätstest zu
machen.


### Weitere Hinweise zu Erweiterungen

*Warnung*: Es kann immer nur ein Prozess auf ein Office-Profil
zugreifen; insbesondere kann *unopkg* nicht valide ausgeführt werden,
wenn bereits ein Office-Prozess gegen das Profil geöffnet ist.

* Anzeige aller installierten Erweiterungen.  Hierbei werden auch die
Identifikatoren angezeigt, die man zum Löschen benötigt.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    unopkg list -env:UserInstallation="file://$temporary_profile"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Extension-Manager außerhalb von Office anzeigen.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    unopkg gui -env:UserInstallation="file://$temporary_profile"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Erweiterung löschen.  Das ist selten nötig, da ohnehin gegen ein
temporäres Profil gearbeitet wird.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ident='…'
    unopkg remove -env:UserInstallation="file://$temporary_profile" "$ident"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Inhalte von Erweiterungen

### Empfohlene Metadaten {#extension-meta}

`description.txt`

:   Kompakte deutschsprachige Beschreibung des Pakets.  Erscheint im
    Extension Manager.  Wird für das Debian-Paket übernommen.

`icon.png`

:   Symbol, per Konvention dasjenige aus dem Beispielmakro, das
    einfach per [Symlink](#symlink) einbinden.  Erscheint im Extension
    Manager.

`description.xml`

:   Beschreibung und Symbol müssen hier entsprechend referenziert
    werden.  Insbesondere auf einen eindeutigen Identifikator
    achten.  Siehe auch [description.xml](https://wiki.openoffice.org/wiki/Documentation/DevGuide/Extensions/Description_of_XML_Elements).


### Einbindung der Benutzer-Dokumentation {#user-doc}

`help/MAK_`*NNN*`_`*Titel*`_UserDoku.pdf`

:   Dies ist ein [Symlink](#symlink) auf das generierte PDF für die
    Benutzerdokumentation.  Der Name folgt traditioneller Konvention.
    Ein sprechender Name ergibt auch Sinn, da der Benutzer bei sich lokal
    das Dokument auch unter diesem Namen sieht.  Das Manifest muss
    entsprechend ergänzt werden
    (*manifest:media-type="application/pdf"*).

`MAK_`*NNN*`/MAK`*NNN*`_Hilfe.star_basic`

:   Hier steht der Code, der die Dokumentation anzeigt.  Das folgende Snippet
    kann als Template dienen:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    option explicit

    sub MAKNNN_Hilfe

      dim pip as object
      dim help as string
      dim oshell as object

      pip = GetDefaultContext.getByName("/singletons/com.sun.star.deployment.PackageInformationProvider")
      help = pip.getPackageLocation(MAKNNN_identifier) & "/help/MAK_NNN_Titel_UserDoku.pdf"

      oshell = createUnoService("com.sun.star.system.SystemShellExecute")
      oshell.execute(help, "", 0)

    end sub
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dieser Code wird nun in ein geeignetes Menü oder eine geeignete Toolbar
gelinkt.  Falls es so etwas nicht gibt, kommt auch das Hilfe-Menü in
Frage, mittels der angestaubten aber nichtsdestotrotz funktionalen
[Add-on Help Integration](https://wiki.openoffice.org/wiki/Documentation/DevGuide/WritingUNO/AddOns/Help_Integration).
Dabei muss in `Addons.xcu` (Manifest-Eintrag ggf. nicht vergessen!) in
etwa folgender Abschnitt ergänzt werden:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    <node oor:name="OfficeHelp">
      <node oor:name="com.sun.star.comp.framework.addon" oor:op="replace">
        <prop oor:name="URL" oor:type="xs:string">
          <value>vnd.sun.star.script:MAK_NNN.MAKNNN_Hilfe.MAKNNN_Hilfe?language=Basic&amp;location=application</value>
        </prop>
        <prop oor:name="Title" oor:type="xs:string">
          <value>Titel (MAK NNN)</value>
        </prop>
        <prop oor:name="Target" oor:type="xs:string">
          <value>_self</value>
        </prop>
      </node>
    </node>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


### Einbindung von Vorlagen {#templates}

Vorlagen brauchen systemweit einen eindeutigen Titel (Attribut *dc:title*
in *meta.xml*), um sich nicht gegenseitig zu verschatten.  Wir erreichen
dies angenähert, indem jeder Titel das Suffix »*(MAK NNN)*« erhält.

`template/MAK_`*NNN_Titel*

:   Hier werden die Vorlagen selbst abgelegt bzw.
    [per Symlink eingebunden](#symlink).

`Paths.xcu`

:   Hier werden die Vorlagen eingebunden (Manifest-Eintrag ggf. nicht
    vergessen!).  Das Snippet dazu ist wie folgt:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  <?xml version='1.0' encoding='UTF-8'?>

  <oor:component-data oor:package="org.openoffice.Office"
      oor:name="Paths" xmlns:install="http://openoffice.org/2004/installation"
      xmlns:oor="http://openoffice.org/2001/registry"
      xmlns:xs="http://www.w3.org/2001/XMLSchema"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

          <node oor:name="Paths">

                  <node oor:name="Template" oor:op="fuse">
                          <node oor:name="InternalPaths">
                                  <node oor:name="%origin%/template" oor:op="fuse"/>
                          </node>
                  </node>

          </node>
  </oor:component-data>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Die offizielle [Dokumentation](#https://wiki.openoffice.org/wiki/Non-code_extensions)
gibt noch zahlreiche Hinweise dazu.  Vorlagen waren auch Thema eines
[Vortrags](#http://conference.libreoffice.org/assets/Conference/Aarhus/Slides/LeifLodhal.pdf)
auf der LibreOffice-Conference 2015.


### Konfigurierbare Parameter {#parameters}

Makros sind in das [benutzerspezifische Konfigurationsmanagement](#user-extensions)
integriert; damit Parameter benutzerspezifisch konfiguriert werden
können, werden sie in `config/parameters.list` aufgelistet.
