
# Troubleshooting

*oprint* oder *ocrypt* laufen immer beim gleichen Dokument in einen Fehler.

:   Wahrscheinlich ist da noch eine Lock-Datei im Dateisystem übrig
    geblieben.  Löschen und nochmals probieren.

