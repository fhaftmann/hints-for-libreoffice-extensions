
# Bevor es losgeht

Diese Dokumentation enthält hilfreiche Hinweise zur Pflege von Office-Extensions.


## Benötigte Pakete

Mit folgender Basisausstattung können fast alle Makros auf
einem Basisclient gebaut werden:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  sudo apt install pandoc python3-odoc python3-uno-util limuxutils \
    devscripts debhelper cdbs p7zip-full
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Voraussetzungen überprüfen

Die Voraussetzungen können überprüft werden, indem der Buildprozess
einmal angestoßen wird, z. B. für die Beispielanwendung:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  ./deploy --extension-only Example_and_Templates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Dokumentation als HTML und PDF

Die Dokumentation, die hier beginnt, kann auch als HTML (mit verlinktem
PDF) erstellt werden.  Dazu einmalig

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  sudo apt install python3-limux-docmaker python3-uno-util aspell-de
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

und dann

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  ./htmlify-development-notes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es empfiehlt sich, dort einfach weiterzulesen.


## Weitere Referenzen

Zahlreiche hier genannte Dinge wurden auf den LibreOffice Conferences
2015 und 2016 vorgestellt und sind im Netz abrufbar:

* <https://www.youtube.com/watch?v=YkqLw814VOo>
* <http://conference.libreoffice.org/assets/Conference/Aarhus/Slides/FlorianHaftmann.pdf>
* <https://www.youtube.com/watch?v=A8N9P_WjjCc>
* <https://conference.libreoffice.org/assets/Conference/Brno/haftmann-user-specific-configuration-management-for-office-extensions.pdf>
