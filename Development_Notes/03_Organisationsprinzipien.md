
# Organisationsprinzipien #

Bei Office-Erweiterungen dreht sich alles überwiegend um die nah
miteinander verwandten
Office-Dokumente, Office-Datenbanken und Office-Erweiterungen,
charakterisiert durch die MIME-Typen *application/vnd.oasis.\**,
*application/vnd.sun.xml.base* und
*application/vnd.openofficeorg.extension*.  Diese werden im folgenden
einfach als Office-Dateien bezeichnet und haben folgende
Charakteristika gemeinsam:

* Sie bestehen aus einem Strauß Dateien mit viel, oftmals schwerfälligem
XML darin.

* Dieser Strauß liegt als Zip-Archiv vor.

* Insbesondere Office-Erweiterungen können rekursiv wiederum
Office-Dokumente enthalten.

Dies macht es schwierig, elementare Entwicklungsmethoden wie
Versionierung auf Office-Dateien anzuwenden.  Wir haben ein paar
Kunstgriffe entwickelt, wie es dennoch möglich ist.


## Tools zur elementaren Beherrschung von Office-Dateien

Diese Kunstgriffe finden sich in den Paketen *python3-odoc* und
*python3-uno-util*.

* *python3-odoc* stellt das Tool *odoc* zur Verfügung, mit dem
Office-Dateien ent- und gepackt werden können:

    * *odoc* arbeitet stand-alone auf Office-Dateien und benötigt
    Office selbst nicht.

    * Entpackte Dateien landen in einem eigenen Unterverzeichnis und
    sind so sauber getrennt.

    * XML-Dokumente werden eingerückt formatiert, mit korrekter
    Behandlung von Whitespace entsprechend dem jeweiligen
    Content-Modell.

    * Star-Basic-Quelltext wird als Plaintext dargestellt.

    * Beim Packen wird der Inhalt gegen das Manifest geprüft.

* Die Tools in *python3-uno-util* operieren über eine Office-Instanz
via UNO:

    * *ocrypt* kann Inhalte und Office-Bibliotheken verschlüsseln.

    * *oprint* ist ein universeller PDF-Ersteller für Office-Dokumente.

*Hinweis*: *python3-odoc* hat bereits eine gewisse Historie hinter sich
und seine Implementierung bietet sicher noch viel Raum für Optimierung.

*Hinweis*: *python3-uno-util* enthält eine kleine
UNO-Abstraktionschicht für Python, die auch in anderem Zusammenhang
nützlich sein kann.


## Die Symlink-Technik {#symlink}

Zip-Archive enthalten keine Symlinks:  beim Packen wird statt eines
Symlinks das Ziel des Symlinks eingebunden.  Dies wird von uns gezielt
ausgenutzt:

* Gemeinsame Dateien.  Über Symlinks lassen sich Dateien (z. B. Icons,
Bibliotheken) über Office-Dateien in den Sourcen sharen und auf dem
gleichen Stand halten.

* Linken von Build-Ergebnissen.  Erweiterungen können anderes Material
enthalten, das wiederum erst gepackt oder gebaut werden muss: Vorlagen,
Java-Archive etc.  Dies kann dadurch erreicht werden, dass in den
Sourcen einer Erweiterung ein Symlink angelegt wird, der auf eine Datei
zeigt, die erst durch einen vorherigen Build erzeugt wird.  Beim Build
der Erweiterung wird dann statt des Symlinks das referenzierte
Build-Ergebnis eingebunden;  solange dieses Build-Ergebnis nicht
vorliegt, bleibt der Symlink »dangling«.
