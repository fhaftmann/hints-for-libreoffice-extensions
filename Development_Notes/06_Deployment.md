
# Bereitstellung {#deployment}

## Versionierungsschema {#versioning}

Traditionell tragen Makros Versionen aus drei Komponenten *X.Y.Z*.
In heutiger Zeit werden *X* und *Y* für Major- und Minor-Versionsnummer
verwendet.  *Z* ist ein Platzhalter, um eine neue Bereitstellung einer
im Kern unveränderten Version machen zu können.  Es ist daher
empfehlenswert, nur *X.Y* in offizieller Dokumentation etc. zu
verwenden.


## Von den Sourcen zur fertigen Bereitstellung

Für jedes Makro wird eine fertige Bereitstellung in folgenden Schritten
erreicht:

* Aus den Sourcen werden die [Erweiterung](#build-extension) und
das Zip-Archiv mit den Admin-Dateien gepackt (»*extension build*«).

* Dann wird ein Debian-Source-Paket gebaut (»*deliverable build*«).


## Artefakte erstellen

### Metadaten für Debian-Pakete

Zum Erstellen der Debian-Source-Pakete müssen die folgenden
Umgebungsvariablen gesetzt sein:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  export DEBEMAIL='<your.name>@muenchen.de'
  export DEBFULLNAME='<Your Name>'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


### Bau der Artefakte

Die Artefakte werden mittels

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  ./deploy
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

erstellt.  Die Zielverzeichnisse für Artefakte können über Parameter
gewählt werden, dazu `./deploy --help` befragen.
