'''
    Build target platform.
'''

# Copyright 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['default']


from os import path

from mopynaco.Record import Record

from . import aux


class Platform(Record('distribution', 'extensions', 'uno_path',
  'dependencies', 'standards', 'debhelper_version', 'copyright', 'epoch',
  'package_prefix')): pass

default = Platform(
  distribution = 'focal',
  extensions = 'usr/lib/libreoffice/share/extensions',
  uno_path = '/usr/lib/libreoffice/program',
  dependencies = ['libreoffice'],
  standards = '4.5.0',
  debhelper_version = '7.0.50',
  copyright = 'IT@m <v-itm-i23@muenchen.de>',
  epoch = '0',
  package_prefix = 'libreoffice-extension-'
)
