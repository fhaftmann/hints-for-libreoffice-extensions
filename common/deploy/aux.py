'''
    Auxiliary.
'''

# Copyright 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['info', 'read_file', 'write_file', 'redirect_symlink', 'temporary_directory']


from contextlib import contextmanager
from os import path
import os
import subprocess
import tempfile
import sys


exec_zipcrypt = 'zipcrypt'


def info(txt):

    print(txt, file = sys.stderr)


def read_file(loc):

    with open(loc, encoding = 'utf-8') as reader:
        content = reader.read()
    return content


def write_file(loc, content):

    with open(loc, 'w', encoding = 'utf-8') as writer:
        writer.write(content)


def remove_if_symlink(loc):

    if path.islink(loc):
        os.unlink(loc)


def redirect_symlink(target, loc):

    remove_if_symlink(loc)
    if path.exists(loc):
        raise IOError('File exists: "{}"'.format(loc))
    os.symlink(target, loc)


def encrypt_zip(password, src, dst):

    mode = os.stat(src).st_mode
    subprocess.check_call([exec_zipcrypt, password, src, dst],
      stdout = subprocess.DEVNULL)
    os.chmod(dst, mode)


@contextmanager
def temporary_directory(suffix = '', prefix = 'tmp', dir = None):

    def permissive_rmdir(loc):
        try:
            os.rmdir(loc)
        except BaseException:
            pass

    loc = tempfile.mkdtemp(suffix, prefix, dir)
    try:
        yield loc
    except BaseException:
        permissive_rmdir(loc)
        raise
    else:
        os.rmdir(loc)
