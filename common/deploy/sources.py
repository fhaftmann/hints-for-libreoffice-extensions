'''
    Classification and processing of sources.
'''

# Copyright 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['base_name_for', 'ident_name_for', 'clear_name_for',
  'the_version', 'prepare',
  'loc_readonly', 'loc_documents', 'loc_extension', 'loc_admin',
  'name_password_libs', 'name_password_content']


import re
from os import path
import os
import fnmatch
import shutil

from odoc import mime, find, rich
from odoc import version as version_reader

from uno_util import document

from .aux import info, temporary_directory


loc_readonly = 'readonly'
loc_documents = 'documents'
loc_extension = 'extension'
loc_admin = 'admin'
name_password_libs = 'crypt/libs.pwd'
name_password_content = 'crypt/content.pwd'


def base_name_for(loc):

    return path.basename(path.abspath(loc))


re_name = re.compile('^MAK_(\d+)_(.*)$')

def ident_name_for(loc):

    base_name = base_name_for(loc)
    match = re_name.match(base_name)
    return 'MAK_' + match.group(1) if match else base_name


def clear_name_for(loc):

    base_name = base_name_for(loc)
    match = re_name.match(base_name)
    return match.group(1) + '_' + match.group(2) if match else base_name


def the_version(loc):

    src = path.join(loc, loc_extension)
    version = version_reader.for_directory(src)

    if version is None:
        raise Exception('No version found in {}'.format(src))

    return version


def is_template(loc):

    return mime.guess_type(loc) in [mime.type.text_template, mime.type.spreadsheet_template,
      mime.type.presentation_template, mime.type.graphics_template, mime.type.chart_template,
      mime.type.formula_template, mime.type.image_template]


def prepare(desktop, src):

    src_doc_root = path.join(src, loc_readonly)
    for src_doc, name in sorted((entry.path, entry.name)
      for entry in os.scandir(src_doc_root)):
        if fnmatch.fnmatchcase(name, '*.odt'):
            info('Printing {}…'.format(name))
            document.create_pdf(desktop, src_doc, path.splitext(src_doc)[0] + '.pdf')

    loc_password_libs = path.join(src, name_password_libs)
    if not path.exists(loc_password_libs):
        loc_password_libs = None
    loc_password_content = path.join(src, name_password_content)
    if not path.exists(loc_password_content):
        loc_password_content = None
    unbundled_dirs = [loc
      for loc, _ in find.open_document_directories(path.join(src, loc_documents))] \
      if path.isdir(path.join(src, loc_documents)) else []
    for loc in unbundled_dirs:
        rich.bundle(loc, prebuild = True)
        doc_src = rich.file_for_directory(loc)
        name = path.basename(doc_src)
        with temporary_directory() as loc_tmp:
            dst_tmp = path.join(loc_tmp, name)
            if loc_password_libs is not None and not mime.is_database(mime.guess_type(doc_src)):
                info('Encrypting libraries of {}…'.format(name))
                with open(loc_password_libs) as reader:
                    password = reader.read().rstrip()
                document.encrypt_libs(desktop, password, doc_src, dst_tmp)
                shutil.move(dst_tmp, doc_src)
            if loc_password_content is not None:
                info('Encrypting content of {}…'.format(name))
                with open(loc_password_content) as reader:
                    password = reader.read().rstrip()
                document.encrypt(desktop, password, doc_src, dst_tmp)
                shutil.move(dst_tmp, doc_src)

    rich.bundle(path.join(src, loc_extension), prebuild = True)
