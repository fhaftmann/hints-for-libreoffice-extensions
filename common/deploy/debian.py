'''
    Debian packaging.
'''

# Copyright 2015 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['get_author_from_context', 'build_package']


from os import path
import os
import shutil
import subprocess
from datetime import datetime

from mopynaco.Record import Record

from odoc import rich

from .aux import info, read_file, write_file
from . import sources


class Author(Record('name', 'email')): pass


def get_author_from_context():

    name = os.getenv('DEBFULLNAME')
    email = os.getenv('DEBEMAIL')

    if name is None:
        raise Exception('No debian maintainer name.')
    if email is None:
        raise Exception('No debian maintainer mail.')

    return Author(name = name, email = email)


def build_package(src, version, dst, platform, deb_author):

    info('Building debian source package…')
    pkg_name = platform.package_prefix + \
      sources.clear_name_for(src).replace('_', '-').lower()
    pkg_version = version
    deb_version = platform.epoch + ':' + pkg_version if platform.epoch != '0' else pkg_version
    dst_pkg = path.join(dst, pkg_name + '_' + pkg_version)
    dst_pkg_extension = path.join(dst_pkg, 'extension')

    if path.exists(dst_pkg):
        shutil.rmtree(dst_pkg)
    os.makedirs(dst_pkg, exist_ok = True)

    os.makedirs(path.join(dst_pkg, 'debian'), exist_ok = True)
    rich.extract(path.join(src, sources.loc_extension) + '.oxt',
      target = dst_pkg_extension)

    subprocess.check_call(['dch', '--create', '--package=' + pkg_name, '--newversion=' + deb_version,
      'Packaged extension ' + pkg_name + ' for LibreOffice'], cwd = dst_pkg)
    subprocess.check_call(['dch', '--release', '--distribution', platform.distribution, '--force-distribution', ''],
      cwd = dst_pkg, stderr = subprocess.DEVNULL)

    default_description = 'Extension {} for LibreOffice'.format(pkg_name)
    description = '\n'.join([ ' ' + line + '\n'
      for line in read_file(path.join(dst_pkg_extension, 'description.txt')).rstrip().split('\n') ])

    control = '''Source: {}
Section: lhm/editors
Priority: optional
Maintainer: {} <{}>
Build-Depends: debhelper (>= {})
Standards-Version: {}

Package: {}
Architecture: all
Depends: {}
Description: {}
{}
'''.format(pkg_name, deb_author.name, deb_author.email, platform.debhelper_version, platform.standards, pkg_name,
       '${misc:Depends}, ' + ', '.join(platform.dependencies),
       default_description, description)
    write_file(path.join(dst_pkg, 'debian/control'), control)

    write_file(path.join(dst_pkg, 'debian/install'),
      '{}/* {}\n'.format('extension',
        path.join(platform.extensions, pkg_name)))

    rules = '''#!/usr/bin/make -f

# export DH_VERBOSE=1

%:
	dh $@
'''
    write_file(path.join(dst_pkg, 'debian/rules'), rules)

    write_file(path.join(dst_pkg, 'debian/compat'), '7')

    write_file(path.join(dst_pkg, 'debian/copyright'), '''
Copyright {} {}

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished
to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.'''.format(datetime.now().year, platform.copyright))

    write_file(path.join(dst_pkg, 'debian/lintian-overrides'), '{}: dir-or-file-in-opt\n'.format(pkg_name))

    subprocess.check_call(['dpkg-source', '-q', '-b', '.'], cwd = dst_pkg)
